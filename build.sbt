name := "ReadingDebt"

version := "0.1"

scalaVersion := "2.12.5"

libraryDependencies +=  "org.scalaj" %% "scalaj-http" % "2.3.0"

libraryDependencies += "com.typesafe.play" %% "play-ws-standalone-xml" % "2.0.0-M1"
libraryDependencies += "com.typesafe.play" %% "play-ws-standalone-json" % "2.0.0-M1"

libraryDependencies += "com.typesafe" % "config" % "1.3.2"
