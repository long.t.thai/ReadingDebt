package com.readingdebt.util

object TextUtil {
  private val TAG_EXPRESSION = "<[^>]+>";

  def removeTags(text: String): String = {
    return text.replaceAll(TAG_EXPRESSION, "")
  }

  def removeNewLines(text: String): String = {
    return text.split("\n").filter(!_.trim.isEmpty).mkString(" ")
  }

  def countWords(text: String): Int = {
    return text.split("\\W").size
  }
}
