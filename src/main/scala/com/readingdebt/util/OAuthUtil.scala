package com.readingdebt.util

import scalaj.http.{Http, HttpRequest, Token}

object OAuthUtil {

  def makeRequest(consumer: Token, url: String,
                  params: Seq[(String, String)]): HttpRequest = {
    val response = Http(url)
      .postForm(params)
      .oauth(consumer)

    validateResponse(response)

    return response
  }

    def makeRequest(consumer: Token, accessToken: Token, url: String,
                  params: Seq[(String, String)]): HttpRequest = {
    val response = Http(url)
      .postForm(params)
      .oauth(consumer, accessToken)

    validateResponse(response)

    return response
  }

  private def validateResponse(response: HttpRequest) = {
    val responseCode = response.asParamMap.code

    if (responseCode != 200) {
      throw new RuntimeException("Response code %s".format(responseCode))
    }
  }
}
