package com.readingdebt.config

import com.typesafe.config.ConfigFactory


object Account {
  private val conf = ConfigFactory.load()
  val INSTAPAPER_CONSUMER_ID = conf.getString("INSTAPAPER_CONSUMER_ID")
  val INSTAPAPER_CONSUMER_SECRET = conf.getString("INSTAPAPER_CONSUMER_SECRET")
  val INSTAPAPER_USERNAME = conf.getString("INSTAPAPER_USERNAME")
  val INSTAPAPER_PASSOWRD = conf.getString("INSTAPAPER_PASSOWRD")
}
