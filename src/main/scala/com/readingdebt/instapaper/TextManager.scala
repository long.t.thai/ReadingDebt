package com.readingdebt.instapaper

import com.readingdebt.util.OAuthUtil
import com.readingdebt.util.TextUtil.{ removeTags, removeNewLines }
import scalaj.http.Token

object TextManager {

  private val GET_TEXT_URL = "https://www.instapaper.com/api/1/bookmarks/get_text"

  def getText(consumer: Token, accessToken: Token,
              bookmarkId: String): String = {
    val params = Seq("bookmark_id" -> bookmarkId)
    val httpText = OAuthUtil
      .makeRequest(consumer, accessToken, GET_TEXT_URL, params)
      .asString
      .body

    return removeNewLines(removeTags(httpText))
  }
}
