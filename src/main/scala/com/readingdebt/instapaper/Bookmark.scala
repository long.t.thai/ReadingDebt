package com.readingdebt.instapaper

case class Bookmark(bookmarkId: String, title: String, url: String, instaPaperUrl: String)
