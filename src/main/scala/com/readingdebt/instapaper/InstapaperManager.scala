package com.readingdebt.instapaper

import com.readingdebt.config.Account.{INSTAPAPER_CONSUMER_ID, INSTAPAPER_CONSUMER_SECRET, INSTAPAPER_PASSOWRD, INSTAPAPER_USERNAME}
import com.readingdebt.util.{OAuthUtil, TextUtil}
import scalaj.http.Token

object InstapaperManager {

  private val ACCESS_TOKEN_URL = "https://www.instapaper.com/api/1/oauth/access_token"

  private val params = Seq("x_auth_mode" -> "client_auth",
    "x_auth_username" -> INSTAPAPER_USERNAME,
    "x_auth_password" -> INSTAPAPER_PASSOWRD)

  def main(args: Array[String]): Unit = {
    val consumer = Token(INSTAPAPER_CONSUMER_ID, INSTAPAPER_CONSUMER_SECRET)
    val token = getToken(consumer)
//    BookmarkManager.getBookmarks(consumer, token).foreach(println)
    val text = TextManager.getText(consumer, token, "967995960")
    println(TextUtil.countWords(text))
  }

  def getToken(consumer: Token): Token = {
    return OAuthUtil.makeRequest(consumer, ACCESS_TOKEN_URL, params)
        .asToken
        .body
  }
}
