package com.readingdebt.instapaper

import com.readingdebt.util.OAuthUtil
import play.api.libs.json.{JsArray, Json}
import scalaj.http.Token

import scala.annotation.tailrec

object BookmarkManager {

  private val BOOKMARK_URL = "https://www.instapaper.com/api/1/bookmarks/list"
  private val READ_URL = "https://www.instapaper.com/read/"

  def getBookmarks(consumer: Token, accessToken: Token): List[Bookmark] = getBookmarksRecursively(consumer, accessToken, List(), List())

  @tailrec
  private def getBookmarksRecursively(consumer: Token, accessToken: Token, existingBookmarkIds: List[String],
                                      result: List[Bookmark]): List[Bookmark] = {
    val bookmarks = getBookmarks(consumer, accessToken, existingBookmarkIds)

    if (bookmarks.isEmpty) {
      return result
    }

    val updatedResult = result ::: bookmarks
    val updatedExistingBookmarkIds = existingBookmarkIds ::: bookmarks.map(b => b.bookmarkId)
    return getBookmarksRecursively(consumer, accessToken, updatedExistingBookmarkIds, updatedResult)
  }

  private def getBookmarks(consumer: Token, accessToken: Token, existingBookmarkIds: List[String]): List[Bookmark] = {
    val haveParam = existingBookmarkIds.mkString(",")
    val params = Seq("limit" -> "500", "have" -> haveParam)
    val response = OAuthUtil.makeRequest(consumer, accessToken, BOOKMARK_URL, params)
    val bookmarks = parseBookmarkResponse(response.asString.body)
    return bookmarks
  }

  private def parseBookmarkResponse(response: String): List[Bookmark] = {
    val json = Json.parse(response)
    val array = json.as[JsArray].value
    val bookmarkJons = array.filter(n => !(n \ "hash").asOpt[String].isEmpty)

    return bookmarkJons.map(node => {
      val bookmarkId = (node \ "bookmark_id").as[BigDecimal].bigDecimal.toPlainString
      val title = (node \ "title").as[String]
      val url = (node \ "url").as[String]
      new Bookmark(bookmarkId, title, url, READ_URL + bookmarkId)
    }).toList
  }
}
